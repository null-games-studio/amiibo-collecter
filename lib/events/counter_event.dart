enum CounterEvent { increase, decrease }
abstract class CounterEventCallback{
  onIncrease();
  onDecrease();
}
