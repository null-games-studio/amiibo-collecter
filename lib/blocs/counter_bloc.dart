import 'package:amiibo_collector/events/counter_event.dart';
import 'package:amiibo_collector/states/counter_state.dart';
import 'package:bloc/bloc.dart';

class CounterBloc extends Bloc<CounterEvent, CounterState> implements CounterEventCallback {
  @override
  get initialState => CounterState.initial();

  @override
  Stream<CounterState> mapEventToState(CounterEvent event) async* {
    switch (event) {
      case CounterEvent.increase:
        yield CounterState(counter: currentState.counter + 1);
        break;
      case CounterEvent.decrease:
        yield CounterState(counter: currentState.counter - 1);
        break;
    }
  }

  @override
  onDecrease() {
    dispatch(CounterEvent.decrease);
  }

  @override
  onIncrease() {
    dispatch(CounterEvent.increase);
  }
}
