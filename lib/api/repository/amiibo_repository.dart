import 'package:amiibo_collector/api/provider/amiibo_provider.dart';
import 'package:amiibo_collector/model/response/amiibo_response.dart';

class AmiiboRepository {
  AmiiboApiProvider _apiProvider = AmiiboApiProvider();

  Future<AmiiboResponse> getAmiibos() {
    return _apiProvider.getAmiibos();
  }
}
