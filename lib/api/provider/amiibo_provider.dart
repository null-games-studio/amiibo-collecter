import 'package:amiibo_collector/model/response/amiibo_response.dart';
import 'package:dio/dio.dart';

class AmiiboApiProvider {
  final String _endpoint = "https://www.amiiboapi.com/api/amiibo/";
  final Dio _dio = Dio();

  Future<AmiiboResponse> getAmiibos() async {
    try {
      Response response = await _dio.get(_endpoint);
      return AmiiboResponse.fromJson(response.data);
    } catch (error, stacktrace) {
      print("Exception occured: $error stackTrace: $stacktrace");
      return AmiiboResponse.withError("$error");
    }
  }
}
