/*
  "amiibo": [
        {
            "amiiboSeries": "Legend Of Zelda",
            "character": "Zelda",
            "gameSeries": "The Legend of Zelda",
            "head": "01010000",
            "image": "https://raw.githubusercontent.com/N3evin/AmiiboAPI/master/images/icon_01010000-03520902.png",
            "name": "Toon Zelda - The Wind Waker",
            "release": {
                "au": "2016-12-03",
                "eu": "2016-12-02",
                "jp": "2016-12-01",
                "na": "2016-12-02"
            },
            "tail": "03520902",
            "type": "Figure"
        }
* */

class AmiiboKeys {
  static String amiiboSeries = "amiiboSeries";
  static String character = "character";
  static String gameSeries = "gameSeries";
  static String head = "head";
  static String image = "image";
  static String name = "name";
  static String release = "release";
}

class ReleaseKeys {
  static String au = "au";
  static String jp = "jp";
  static String eu = "eu";
  static String na = "na";
}

class Release {
  final auDate;
  final euDate;
  final jpDate;
  final naDate;

  Release(this.auDate, this.euDate, this.jpDate, this.naDate);

  Release.fromJson(Map<String, dynamic> json)
      : auDate = json[ReleaseKeys.au],
        euDate = json[ReleaseKeys.eu],
        jpDate = json[ReleaseKeys.jp],
        naDate = json[ReleaseKeys.na];
}

class Amiibo {
  final String amiiboSeries;
  final String character;
  final String gameSeries;
  final String head;
  final String image;
  final String name;
  final Release release;

  Amiibo(this.amiiboSeries, this.character, this.gameSeries, this.head,
      this.image, this.name, this.release);

  Amiibo.fromJson(Map<String, dynamic> json)
      : amiiboSeries = json[AmiiboKeys.amiiboSeries],
        character = json[AmiiboKeys.character],
        gameSeries = json[AmiiboKeys.gameSeries],
        head = json[AmiiboKeys.head],
        image = json[AmiiboKeys.image],
        name = json[AmiiboKeys.name],
        release = Release.fromJson(json[AmiiboKeys.release]);

}
