import 'package:amiibo_collector/model/amiibo.dart';

class AmiiboResponse {
  final List<Amiibo> amiibo;
  final String error;

  AmiiboResponse(this.amiibo, this.error);

  AmiiboResponse.fromJson(Map<String, dynamic> json)
      : amiibo = (json["amiibo"] as List)
            .map((i) => new Amiibo.fromJson(i))
            .toList(),
        error = "";

  AmiiboResponse.withError(String errorValue)
      : amiibo = List(),
        error = errorValue;
}
